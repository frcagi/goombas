﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    List<Goomba> goombaList = new List<Goomba>();
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }

private int countDead()
    {
        //variable acumulativa
        int nDead = 0;

        for (int i=0; i<goombaList.Count; i++) {
            //Si esta vivo
            if (!goombaList[i].isALive())
            {
                nDead++;
            }
        }
        return nDead;
    
    }
    List <int> getPositions()
    {
        List<int> posicionesX = new List<int>();
        for (int i=0; i<goombaList.Count; i++) {
            if (goombaList[i].isALive()) {
                posicionesX.Add(goombaList[i].x);
            }
        }
        return posicionesX;
    }
}
